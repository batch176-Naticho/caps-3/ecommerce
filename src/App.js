import './App.css';
import data from './mockData/data';
import Home from './components/Home';
import Header from './components/Header';
import Cart from './components/Cart';
import Products from './components/Products';
import Signin from './pages/Signin';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';
import { useState } from 'react';
import { UserProvider } from './UserContext';
import { BrowserRouter, Routes, Route  } from 'react-router-dom';


function App() {

  //store and validate user information
  const [ user, setUser ] = useState({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  //clear localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  const {productItems} = data;
  const [cartItems, setCartItems] = useState([]);


  const onAdd = (productItems) => {
    const exist = cartItems.find(x => x.id === productItems.id)
    if(exist){
      setCartItems(cartItems.map(x => x.id === productItems.id ? {...exist, qty: exist.qty + 1}: x))
    }else{
      setCartItems([...cartItems, {...productItems, qty: 1}])
    }
  };

  const onRemove = (productItems) => {
    const exist = cartItems.find((x) => x.id === productItems.id);
    if(exist.qty === 1){
      setCartItems(cartItems.filter((x) => x.id !== productItems.id))
    }else{
      setCartItems(cartItems.map(x => x.id === productItems.id ? {...exist, qty: exist.qty - 1}: x))
    }
  }

  const onClear = () => {
    setCartItems([]);
  }

  return (
    <UserProvider value = {{ user, setUser, unsetUser }} >
      <BrowserRouter >
        < Header countCartItems={cartItems.length}/>    
          <Routes>
            <Route path="/" element={ <Home />} />
            <Route path="/products" element={<Products productItems={productItems} onAdd={onAdd}/>} />
            <Route path="/cart" element={<Cart cartItems={cartItems} onAdd={onAdd} onRemove={onRemove} onClear={onClear}/>}/>
            <Route path="/signin" element={ <Signin />} />
            <Route path="/login" element={ <Login />} />
            <Route path="/logout" element={ <Logout />} />
            <Route path="*" element={<ErrorPage/>}/>
          </Routes> 
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;


