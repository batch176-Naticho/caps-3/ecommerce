import React from 'react'
import UserContext from '../UserContext';
import { useContext } from 'react';



const Cart = (props) => {
  const {cartItems, onAdd, onRemove, onClear} = props;
  const itemsPrice = cartItems.reduce((a, c) => a + c.price * c.qty, 0);
  const taxPrice = itemsPrice * 0.12;
  const shippingPrice = itemsPrice > 10000 ? 0 : 100;
  const totalPrice = itemsPrice + taxPrice + shippingPrice;

  const { user } = useContext(UserContext)

  return (
    <div className='cart-items'>
      <div className='cart-items-header'>Cart Items</div>
      {cartItems.length === 0 && (
        <div className='empty-cart'>Your cart is Empty</div>
      )}

      <div>
        <div className='clear-cart'>
          {cartItems.length >= 1 && (      
                <button className='clear-cart-button' onClick={onClear}>Clear Cart</button>                                     
            )}
        </div>
        {cartItems.map((item) =>(
          <div key={item.id} className="cart-items-list">           
              <img className='cart-items-image' src={item.image} alt={item.name} />                  
              <div className='cart-items-name'>{item.name}</div>
            <div className='cart-items-qty'>
              <button onClick={()=>onRemove(item)} className="remove">-</button>
              <button onClick={()=>onAdd(item)} className="add">+</button>
            </div>
            <div className='cart-items-price'>
              {item.qty} x Php{item.price}
            </div>
          </div>
          
        ))}

        {cartItems.length !== 0 && (
          <div className='total-price-breakdown'>
            <hr></hr>
            <div className='items-price'>
              <div className='price-parameter'>Items Price</div>
              <div>Php{itemsPrice.toFixed(2)}</div>
            </div>
            <div className='tax-price'>
              <div className='price-parameter'>Tax Price</div>
              <div>Php{taxPrice.toFixed(2)}</div>
            </div>
            <div className='shipping-price'>
              <div className='price-parameter'>Shipping Price</div>
              <div>Php{shippingPrice.toFixed(2)}</div>
            </div>
            <div className='total-price'>
              <div className='price-parameter'><strong>Total Price</strong></div>
              <div><strong>Php{totalPrice.toFixed(2)}</strong></div>
            </div>
          </div> 
        )}
        <div className='cart-last-button'>
          <div className='clear-cart'>
            {cartItems.length >= 1 && (   
              <div className='cart-checkout'>     
                <button className='checkout-button'>Checkout</button>
              </div>        
            )}
          </div>
             
          <div>

          </div>
        </div>
      </div>

    </div>
  )
}

export default Cart