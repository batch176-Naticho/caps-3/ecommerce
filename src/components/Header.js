import React from 'react';
import { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Header = (props, setShow) => {

  const {user} = useContext(UserContext)

  const {countCartItems} = props;
    return(
        <div className='Header'>
          <div className='nav-brand'>
            <h2>S & S online</h2>               
          </div>
          <div className='header-links'>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/products">Products</Link>
              </li>

              {(user.accessToken !== null) ?
                <li><Link to="/logout">Logout</Link></li>
                 :
                 <>
                  <li><Link to="/login">Log in</Link></li>
                  <li><Link to="/signin">Sign in</Link></li>    
                 </>        
               }  
              <li>
                <Link to="/cart" className='cart'><FontAwesomeIcon icon={faShoppingCart} ></FontAwesomeIcon>
                {' '}
                 { countCartItems ? (
                  <button className='badge'>{countCartItems}</button>
                 ) : (
                  ''
               )}
                
                </Link>
              </li>
              
            </ul>
          </div>
        </div>
    )
}
export default Header;