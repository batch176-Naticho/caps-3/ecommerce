import React from "react";
import { Link } from 'react-router-dom';



const Home = () => {
    return(
        <div className="main-container">
			<div className="home-writings">
			<div className="name">S & S Online Shop</div>
			<div className="subtitle">"Shopping and Retail"</div>
			<div className="tagline">Shoes for every place you need to Go!</div>
			</div>
			<div className="home-product-view">
				<Link to="/products">
					<button className="home-button">Browse our Products</button>
				</Link>
			</div>
			
		</div>
	
		

    )
}

export default Home;