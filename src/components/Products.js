import React, {useContext} from 'react'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

const Products = (props) => {
  const { user } = useContext(UserContext)

    const { productItems, onAdd } = props;
  return (  
      <div className='products'>
        {productItems.map((productItems) => (
          <div className='product-card'>
            <div>
              <img className='product-img' src={productItems.image} alt={productItems.name}/>
            </div>
            <div>
              <div>
                <h3 className='product-name'>{productItems.name}</h3>
              </div>
              <div className='product-des'>{productItems.description}</div>
              <div className='product-price'>Php{productItems.price}</div>
{/* user login */}
                <div>
                  <>
              { user.accessToken !== null ?
                <button className='add-to-cart-button' onClick={()=>onAdd(productItems)}>Add To Cart</button> 
                :
                <Link to="/Login">
                 <button className='login-to-buy'>Login To Add To Cart</button>
               </Link>
                } 
                </>
                </div>
             
            </div>

          </div>
        ))}

      </div>
  )
}

export default Products

