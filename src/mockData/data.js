

const data = {
    productItems: [
       {
           id:"1",
           image:"../images/img1.jpg" ,
           name: "Nike Air Force 1 Low ",
           price: 7000,
           description: "Sizes: 36-46"
       },
       {
           id:"2",
           image: "../images/img2.jpg" ,
           name: "Adidas Alphabounce Beyond",
           price: 5500,
           description: "Sizes: 36-46, Low(3M)"
       },
       {
        id:"7",
        image:"../images/img7.jpg" ,
        name: "BIRKENSTOCK NEW DESIGN",
        price: 1540,	
        description: "For Women Sizes 35-45"
       },
       {
           id:"3",
           image:"../images/img3.jpg" ,
           name: "Nike SB Dunk Low",
           price: 6500,	
           description: "Sizes: 36-46"
       },
       {
           id:"4",
           image:"../images/img4.jpg" ,
           name: "AIR JORDAN 1 LOW",
           price: 5500,	
           description: "Sizes available 36-46"
       },
       {
           id:"5",
           image:"../images/img5.jpg" ,
           name: "NIKE SB DUNK LOW PRO",
           price: 4500,	
           description: "Sizes 36-46"
       },
       {
           id:"6",
           image:"../images/img6.jpg" ,
           name: "JORDAN AIR ZOOM CMFT",
           price: 6700,	
           description: "Sizes 36-46"
       },
       
       {
           id:"8",
           image:"../images/img8.jpg" ,
           name: "AIR JORDAN 4 For Kids",
           price: 3600,	
           description: "Sizes 25-35"
       },
       {
           id:"11",
           image:"../images/img11.jpg" ,
           name: "COACH Belt/Chest Bag",
           price: 1800,	
           description: "Size 30*16 cm"
       },
   ]
}




export default data;