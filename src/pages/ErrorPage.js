import React from 'react'
import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const ErrorPage = () => {
  return (
    <Row>
			<Col>
				<h1>404 - Page Not Found</h1>
				<p>The requested URL cannot be found in this server.</p>
				<Button variant="primary" as={Link} to="/">Back</Button>
			</Col>
		</Row>
  )
}

export default ErrorPage