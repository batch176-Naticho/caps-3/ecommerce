import React from 'react'
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';



const Login = () => {

const navigate = useNavigate();

const  { user, setUser } = useContext(UserContext)	

const [ email, setEmail ] = useState('');
const [ password, setPassword ] = useState('');


const [ isActive, setIsActive ] = useState(true);

//renders when there is a change in the email and password
useEffect(() => {
    if(email !== '' && password !== '') {
        setIsActive(true);
    } else {
        setIsActive(false);
    }
}, [email, password])

function authentication(e) {
    e.preventDefault();
//trigers the fetch and go to the url to connect to the backend (login)
    fetch ('https://touchstone-ecommerce.herokuapp.com/users/login', {
        method: 'POST',
        headers: {'Content-Type': 'Application/json'},
        body: JSON.stringify({
            email: email,
            password: password
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data)

        if(data.accessToken !== undefined){
            localStorage.setItem('accessToken', data.accessToken);
            setUser({
                accessToken: data.accessToken
            })
            Swal.fire({
                title: 'Welcome',
                icon: 'success',
                text: 'You are now login'
            })

            fetch('https://touchstone-ecommerce.herokuapp.com/users/details', {
                headers: {
                    Authorization: `Bearer ${data.accessToken}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if(data.isAdmin === true ) {
                    localStorage.setItem('isAdmin', data.isAdmin)
                    setUser({
                        isAdmin: data.isAdmin
                    })
                    navigate('/products')
                }else{
                    navigate('/products')
                }
            })
        }else{
            Swal.fire({
                title: 'Ooooops',
                icon: 'error',
                text: 'Invalid email or password'
            })
        }
        setEmail('')
        setPassword('')
    })

}

  return (
    (user.accessToken !== null) ?

		<Navigate to="/products" />
		:
		<Form onSubmit={e => authentication(e)} className="loginForm ">
            <h1 className="login-title">Log In</h1>

			<Form.Group>
				<Form.Label className="login-email">Email Address</Form.Label>
				<Form.Control 
				    type="email"
				    placeholder="Enter email"
				    required
				    value={email}
				    onChange={e => setEmail(e.target.value)}
				    /> 
			</Form.Group>

			<Form.Group>
				<Form.Label className="login-password">Password</Form.Label>
				<Form.Control 
				    type="password"
				    placeholder="Enter your Password"
				    required
				    value={password}
				    onChange={e => setPassword(e.target.value)}
				    />
			</Form.Group>
			{ isActive ?
			<Button variant="secondary" type="submit" className="login-button">
				Log in
			</Button>
			:
			<Button variant="secondary" type="submit" className="login-button" disabled>
				Log in
			</Button>
			}

		</Form>
  )
}

export default Login