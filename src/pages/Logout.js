import React from 'react'
import { useContext, useEffect, setUser } from 'react'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

const Logout = () => {
    const { unsetUser, setUser } = useContext(UserContext);

	//clear the localStorage (userContext)
	unsetUser();

	//set user state to it soriginal value
	useEffect(() => {
		setUser({
			accessToken:null
		})
	}, [])


//to clear local storage to be able to login and register again 
	//localStorage.clear();
  return (
    <Navigate to="/" />
  )
}

export default Logout