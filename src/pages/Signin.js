import React from 'react'
import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';

const Signin = () => {
    const navigate = useNavigate();

	const { user, setUser } = useContext(UserContext)

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');

	//Validation
	const [ isActive, setIsActive ] = useState(true)

	useEffect(() => {
		if((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}	
	}, [ email, password, verifyPassword])

	function registerUser(e) {
		e.preventDefault();

		fetch('https://touchstone-ecommerce.herokuapp.com/users/register',{
			method:'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				email: email,
				password: password,
				verifyPassword: verifyPassword
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			
				if(data.email !== ''){
					localStorage.setItem('email', data.email);
					setUser({
						email:data.email
					})

					Swal.fire({
						title:'Congrats!',
						icon:'success',
						text:'You are now Registered'
					})
					navigate('/login')
				}else{
					Swal.fire({
						title:'Oooops',
						icon:'error',
						text:'Failed to Register'
					})
				};

			setEmail('')
			setPassword('')
			setVerifyPassword('')
		})

	}
  return (
    (user.accessToken !== null) ?

		<Navigate to="/login"/>
		:
		<Form onSubmit={e => registerUser(e)} className="signinForm">
		<h1 className="signin-title">Sign In</h1>
			<Form.Group className='signin-email'>
				<Form.Label className='sigin-eamil'>Email Address</Form.Label>
				<Form.Control 
				type="email"
				placeholder="Enter your email" 
				required
				value={email}
				onChange={e => setEmail(e.target.value)}
				/>
				
				
			</Form.Group>

			<Form.Group>
				<Form.Label className="signin-password">Password</Form.Label>
				<Form.Control 
				type="password"
				placeholder="Enter your password"
				required
				value={password}
				onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label className="sigin-vpassword">Verify Password</Form.Label>
				<Form.Control 
				type="password"
				placeholder="Verify password"
				required
				value={verifyPassword}
				onChange={e => setVerifyPassword(e.target.value)}
				/>
			</Form.Group>

			{isActive ? 
				<Button variant="primary" type="submit" className="signin-button">Submit</Button>
				:
				<Button variant="primary" type="submit" className="signin-button" disabled>Submit</Button>			
			}

		</Form>
  )
}

export default Signin